<?php

namespace App;



class Inventory
{

	public $data = [
		['id' => 0, 'name' => 'Test0', 'amount' => 3, 'Type' => 'Gloves'],
		['id' => 1, 'name' => 'Test1', 'amount' => 2, 'Type' => 'Gloves'],
		['id' => 2, 'name' => 'Test2', 'amount' => 1, 'Type' => 'Gloves'],
		['id' => 3, 'name' => 'Test3', 'amount' => 7, 'Type' => 'Gloves'],
		['id' => 4, 'name' => 'Test4', 'amount' => 4, 'Type' => 'Shirts'],
		['id' => 5, 'name' => 'Test5', 'amount' => 5, 'Type' => 'Shirts'],
		['id' => 6, 'name' => 'Test6', 'amount' => 9, 'Type' => 'Shirts'],
		['id' => 7, 'name' => 'Test7', 'amount' => 7, 'Type' => 'Pants'],
		['id' => 8, 'name' => 'Test8', 'amount' => 8, 'Type' => 'Pants'],
		['id' => 9, 'name' => 'Test9', 'amount' => 6, 'Type' => 'Pants'],
	];



	function addItem($id, $name, $amount, $type)
	{
		$array = $this->data;
		if (in_array($id, array_column($array, 'id'))) {
			foreach ($array as $key => $item) {
				if ($item['id'] == $id && $item['Type'] == $type) {
					$item['amount'] += $amount;
					$item['name'] = $name;
					$array[$key] = $item;

					$result = [
						"msg" => "Item already exists and its amount has been increased",
						"data" => $array[$key],
						"code" => 200
					];
					return json_encode($result);
				} else {
					$result = [
						"msg" => "false input, mismatch between id and type of the item",
						"data" => null,
						"code" => 400
					];
					return json_encode($result);
				}
			}
		} else {
			array_push(
				$array,
				[
					'id' => $id,
					'name' => $name,
					'amount' => $amount,
					'Type' => $type
				]
			);
			$result = [
				"msg" => "New Item added",
				"data" => $array,
				"code" => 200
			];
			return json_encode($result);
		}
	}



	function removeItem($id, $amount, $type, $remove)
	{
		$array = $this->data;
		if (in_array($id, array_column($array, 'id'))) {
			/// item exist
			if ($remove) {
				/// If the user wants to permanently delete the item
				foreach ($array as $key => $item) {
					if ($item['id'] == $id && $item['Type'] == $type) {
						array_splice($array, $key, 1);
						$result = [
							"msg" => "Item deleted",
							"data" => $array,
							"code" => 200
						];
						return json_encode($result);
					} else {
						$result = [
							"msg" => "false input, mismatch between id and type of the item",
							"data" => null,
							"code" => 400
						];
						return json_encode($result);
					}
				}
			} else {
				/// reduce quantity
				foreach ($array as $key => $item) {
					if ($item['id'] == $id && $item['Type'] == $type) {
						if (($item['amount'] - $amount) <= 0) {
							/// If the quantity supplied is greater than the quantity supplied
							/// the item will be deleted	
							array_splice($array, $key, 1);
							$result = [
								"msg" => "Item deleted because no amount left",
								"data" => $array,
								"code" => 200
							];
							return json_encode($result);
						} else {
							/// reduce quantity 				
							$item['amount'] -= $amount;
							$array[$key] = $item;
							$result = [
								"msg" => "Item's amount reduced",
								"data" => $array[$key],
								"code" => 200
							];
							return json_encode($result);
						}
					}
				}

				$result = [
					"msg" => "false input, mismatch between id and type of the item",
					"data" => null,
					"code" => 400
				];
				return json_encode($result);
			}
		} else {
			echo nl2br("Item not exist:");
		}
	}

	function getStock($type)
	{
		$array = $this->data;
		$result = 0;
		if (in_array($type, array_column($array, 'Type'))) {
			foreach ($array as $item) {
				if ($item['Type'] == $type) {
					$result += $item['amount'];
				}
			}

			$result = [
				"msg" => "Success",
				"data" => $result,
				"code" => 200
			];
			return json_encode($result);
		} else {

			$result = [
				"msg" => "This Type isn't exist yet!",
				"data" => $result,
				"code" => 400
			];
			return json_encode($result);
		}
	}
}



$inventoryObject = new Inventory();


//-------------- add new item -----------------
// echo $inventoryObject->addItem(120, 'add test', 2, 'Gloves');

//-------------- add exist id but not same type -----------------
// echo $inventoryObject ->addItem(1, 'add test', 2, 'Pants');

// -------------- add item but the id is exist ------------------
// echo $inventoryObject ->addItem(0, 'add test', 2, 'Gloves');


// -------------- Permanently remove an item ---------------------
// echo $inventoryObject ->removeItem(0, 2, 'Gloves', true);
// -------------- Permanently remove an item but id mismatch type ---------------------
// echo $inventoryObject ->removeItem(0, 2, 'Pants', true);

//-------------- Removing an amount of a specific element -----------------
//-------------- The available quantity is greater than the quantity to be reduced -----------------
// echo $inventoryObject ->removeItem(0, 2, "Gloves", false);

//-------------- The available quantity is less or equal than the quantity to be reduced -----------------
// echo $inventoryObject ->removeItem(0, 3, "Gloves", false);

//-------------- The id is not for this type -----------------
// echo $inventoryObject ->removeItem(0, 3, "Pants", false);

echo $inventoryObject->getStock('Pants');
