<?php


class Solution
{
    public $data = [
        ['id' => 0, 'name' => 'Test', 'amount' => 3,],
        ['id' => 1, 'name' => 'Test', 'amount' => 2,],
        ['id' => 2, 'name' => 'Test', 'amount' => 1,],
        ['id' => 3, 'name' => 'Test', 'amount' => 0,],
        ['id' => 4, 'name' => 'Test', 'amount' => 4,],
        ['id' => 5, 'name' => 'Test', 'amount' => 5,],
        ['id' => 6, 'name' => 'High', 'amount' => 9,],
        ['id' => 7, 'name' => 'Test', 'amount' => 7,],
        ['id' => 8, 'name' => 'Test', 'amount' => 8,],
        ['id' => 9, 'name' => 'Test', 'amount' => 6,],
    ];

    public $highestAmount = [];
    public $totalAmount = 0;
    public $sortedItems = [];
    public $filteredItems = [];

    function highestAmount()
    {
        $array = $this->data;
        $max = 0;
        foreach ($array as $item) {
            if ($item['amount'] > $max) {
                $max = $item['amount'];
                $this->highestAmount = $item;
            }
        }
    }

    function calculateAmount()
    {
        $array = $this->data;

        foreach ($array as $item) {
            $this->totalAmount += $item['amount'];
        }
    }

    function sortItems($sort)
    {
        $array = $this->data;
        usort($array, function ($item1, $item2) use ($sort) {
            if ($sort == "asc") {
                return $item1['amount'] <=> $item2['amount']; //asc sort
            } elseif ($sort == "desc") {
                return $item2['amount'] <=> $item1['amount']; //desc sort
            }
        });
        $this->sortedItems = $array;
    }

    function filterItems($filter)
    {
        $array = $this->data;
        $this->filteredItems = array_filter($array, function ($var) use ($filter) {
            return ($var['amount'] == $filter);
        });
    }
}



$solution = new Solution();

// Item with the highest amount
$solution->highestAmount();
echo 'Item with the highest amount: ' . $solution->highestAmount['name'] . PHP_EOL . "<br>";

// Total amount of items
$solution->calculateAmount();
echo 'Total amount of items: ' . $solution->totalAmount . PHP_EOL . "<br>";

// Items sorted by amount
$solution->sortItems("desc");
echo 'Items sorted by amount: ' . var_export(array_column($solution->sortedItems, 'amount'), true) . PHP_EOL . "<br>";

// Items filtered by amount
$solution->filterItems(8);
echo 'Items filtered by amount: ' . var_export(array_column($solution->filteredItems, 'amount'), true) . PHP_EOL . "<br>";
