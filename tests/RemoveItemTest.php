<?php

use PHPUnit\Framework\TestCase;
use App\Inventory;


class RemoveItemTest extends TestCase
{

    public function testPermanentRemoveItem()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "Item deleted",
            "data" => [
                ['id' => 1, 'name' => 'Test1', 'amount' => 2, 'Type' => 'Gloves'],
                ['id' => 2, 'name' => 'Test2', 'amount' => 1, 'Type' => 'Gloves'],
                ['id' => 3, 'name' => 'Test3', 'amount' => 7, 'Type' => 'Gloves'],
                ['id' => 4, 'name' => 'Test4', 'amount' => 4, 'Type' => 'Shirts'],
                ['id' => 5, 'name' => 'Test5', 'amount' => 5, 'Type' => 'Shirts'],
                ['id' => 6, 'name' => 'Test6', 'amount' => 9, 'Type' => 'Shirts'],
                ['id' => 7, 'name' => 'Test7', 'amount' => 7, 'Type' => 'Pants'],
                ['id' => 8, 'name' => 'Test8', 'amount' => 8, 'Type' => 'Pants'],
                ['id' => 9, 'name' => 'Test9', 'amount' => 6, 'Type' => 'Pants'],
            ],
            "code" => 200
        ];
        $result = $inventory->removeItem(0, 2, 'Gloves', true);

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testPermanentRemoveItemIdMismatchType()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "false input, mismatch between id and type of the item",
            "data" => null,
            "code" => 400
        ];
        $result = $inventory->removeItem(0, 2, 'Pants', true);

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testRemoveItemReduceAmount()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "Item's amount reduced",
            "data" => [
                "id" => 0,
                "name" => "Test0",
                "amount" => 1,
                "Type" => "Gloves"
            ],
            "code" => 200
        ];
        $result = $inventory->removeItem(0, 2, "Gloves", false);

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testRemoveItemAmountBeenEmpty()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "Item deleted because no amount left",
            "data" => [
                ['id' => 1, 'name' => 'Test1', 'amount' => 2, 'Type' => 'Gloves'],
                ['id' => 2, 'name' => 'Test2', 'amount' => 1, 'Type' => 'Gloves'],
                ['id' => 3, 'name' => 'Test3', 'amount' => 7, 'Type' => 'Gloves'],
                ['id' => 4, 'name' => 'Test4', 'amount' => 4, 'Type' => 'Shirts'],
                ['id' => 5, 'name' => 'Test5', 'amount' => 5, 'Type' => 'Shirts'],
                ['id' => 6, 'name' => 'Test6', 'amount' => 9, 'Type' => 'Shirts'],
                ['id' => 7, 'name' => 'Test7', 'amount' => 7, 'Type' => 'Pants'],
                ['id' => 8, 'name' => 'Test8', 'amount' => 8, 'Type' => 'Pants'],
                ['id' => 9, 'name' => 'Test9', 'amount' => 6, 'Type' => 'Pants'],
            ],
            "code" => 200
        ];
        $result = $inventory->removeItem(0, 3, "Gloves", false);

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testRemoveItemIdNotSameType()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "false input, mismatch between id and type of the item",
            "data" => null,
            "code" => 400
        ];
        $result = $inventory->removeItem(0, 3, "Pants", false);

        $this->assertEquals(json_encode($expected), $result);
    }
}
