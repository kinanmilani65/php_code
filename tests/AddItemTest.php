<?php

use PHPUnit\Framework\TestCase;
use App\Inventory;


class AddItemTest extends TestCase
{

    public function testAddNewItem()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "New Item added",
            "data" => [
                ['id' => 0, 'name' => 'Test0', 'amount' => 3, 'Type' => 'Gloves'],
                ['id' => 1, 'name' => 'Test1', 'amount' => 2, 'Type' => 'Gloves'],
                ['id' => 2, 'name' => 'Test2', 'amount' => 1, 'Type' => 'Gloves'],
                ['id' => 3, 'name' => 'Test3', 'amount' => 7, 'Type' => 'Gloves'],
                ['id' => 4, 'name' => 'Test4', 'amount' => 4, 'Type' => 'Shirts'],
                ['id' => 5, 'name' => 'Test5', 'amount' => 5, 'Type' => 'Shirts'],
                ['id' => 6, 'name' => 'Test6', 'amount' => 9, 'Type' => 'Shirts'],
                ['id' => 7, 'name' => 'Test7', 'amount' => 7, 'Type' => 'Pants'],
                ['id' => 8, 'name' => 'Test8', 'amount' => 8, 'Type' => 'Pants'],
                ['id' => 9, 'name' => 'Test9', 'amount' => 6, 'Type' => 'Pants'],
                ['id' => 200, 'name' => 'add test', 'amount' => 2, 'Type' => 'Gloves'],
            ],
            "code" => 200
        ];
        $result = $inventory->addItem(200, 'add test', 2, 'Gloves');

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testAddItemIdNotSameType()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "false input, mismatch between id and type of the item",
            "data" => null,
            "code" => 400
        ];
        $result = $inventory->addItem(1, 'add test', 2, 'Pants');

        $this->assertEquals(json_encode($expected), $result);
    }

    public function testAddItemExist()
    {
        $inventory = new Inventory();
        $expected = [
            "msg" => "Item already exists and its amount has been increased",
            "data" => [
                "id" => 0,
                "name" => "add test",
                "amount" => 5,
                "Type" => "Gloves"
            ],
            "code" => 200
        ];
        $result = $inventory->addItem(0, 'add test', 2, 'Gloves');

        $this->assertEquals(json_encode($expected), $result);
    }
}
